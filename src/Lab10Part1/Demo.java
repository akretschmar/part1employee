package Lab10Part1;

import java.util.Calendar;

/**
 *
 * @author akret
 */
public class Demo {

    public static void main(String[] args) {
        
        Calendar dateJoin = Calendar.getInstance();
        dateJoin.set(2022, 0, 22);
        
        Employee em1 = new Employee("001", "Kelly", dateJoin.getTime(), 50000 );
        Tool tool = new Tool();
        
        System.out.println("Is promotion due: " + tool.isPromotionDueThisYear(em1, true));
        System.out.println("Taxes for the year: " + tool.calcIncomeTaxForCurrentYear(em1, 0.3));
                
    }
    
}
