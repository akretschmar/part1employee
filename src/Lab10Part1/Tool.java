
package Lab10Part1;

import java.util.Calendar;


/**
 *
 * @author akret
 */
public class Tool{
    
    
    public double calcIncomeTaxForCurrentYear(Employee employee, double taxPercentage){
        return  employee.getEmployeeSalary()*taxPercentage;
    }
    
    public boolean isPromotionDueThisYear(Employee employee, boolean goodPerformance){
        Calendar dateJoined = Calendar.getInstance();
        dateJoined.setTime(employee.getEmployeeDateOfJoining());
        dateJoined.add(Calendar.YEAR, 1);  
        
        Calendar today = Calendar.getInstance();
        
        return today.after(dateJoined) && goodPerformance;
        
        
       
    }
}
