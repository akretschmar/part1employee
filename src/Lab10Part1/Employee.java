
package Lab10Part1;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author akret
 */
public class Employee {
    private String employeeId;
    private String name;
    private Date dateOfJoining;
    private double salary;
    
    public Employee(){}
    
    public Employee(String employeeId, String name, Date dateOfJoining, double salary){
          this.employeeId =  employeeId;
          this.name = name;
          this.dateOfJoining = dateOfJoining;
          this.salary = salary;
    }
    
    public String getEmployeeId(){
        return this.employeeId;
    }
    
    public String getEmployeeName(){
        return this.name;
    }
    
    public Date getEmployeeDateOfJoining(){
        return this.dateOfJoining;
    }
    public double getEmployeeSalary(){
        return this.salary;
    }
    
    
}
